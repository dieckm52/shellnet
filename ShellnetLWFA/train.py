#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 17:27:48 2021

@author: backdirneis
"""


import wandb
import torch
import time
import torch.nn as nn
import math
import os.path
import argparse
from torch import autograd
from pathlib import Path
from model import ShellAuto
from dataset import Dataset
from dataloader import create_dataloaders
from plotandsave import plot_results, save_results, plot_data
from helpfunctions import Binned_mse, bin_pcs

def get_paras(parser):
    """
    Get all the user input parameters

    Parameters
    ----------
    parser : Parser

    Returns
    -------
    paras : Namespace Object with all the parameters

    """
    
    parser.add_argument('--batchsize', type=int, default=4)
    parser.add_argument('--first_stamp', type=int, default=0)
    parser.add_argument('--number_per_file', type=int, default=16)
    parser.add_argument('--num_points', type=int, default=2048)
    parser.add_argument('--learning_rate', type=float, default=0.0001)
    parser.add_argument('--gamma', type=float, default=1.0)
    parser.add_argument('--num_downsampling', type=int, default=3)
    parser.add_argument('--num_conv_samesize', type=int, default=0)
    parser.add_argument('--c_filters', type=int, default=512)
    parser.add_argument('--min_neighbors', type=int, default=8)    
    parser.add_argument('--start_query', type=int, default=512)    
    parser.add_argument('--latent_dim', type=int, default=64)
    parser.add_argument('--linear_layers', type=int, default=2)  ## IMPORTANT: The factor by which filters in linear layers is reduced; not higher than 8
    parser.add_argument('--no-batchnorm', dest='batchnorm', action='store_false')
    parser.add_argument('--n_epochs', type=int, default=1)
    parser.add_argument('--no-normalize', dest='normalize', action='store_false')
    parser.add_argument('--taurus', dest='work_local', action='store_false')
    parser.add_argument('--fast_run', dest='fast_run', action='store_true')
    parser.add_argument('--in_team', dest='in_team', action='store_true')
    parser.add_argument('--wandb', dest='wandb', action='store_true')
    parser.add_argument('--binned_loss', dest='binned_loss', action='store_true')
    parser.add_argument('--no-skip_connect', dest='skip_connect', action='store_false')
    parser.add_argument('--start_from_check', dest='start_from_check', action='store_true')
    parser.add_argument('--easy_pcs', dest='easy_pcs', action='store_true')
    paras = parser.parse_args()
    
    if paras.work_local:
        vars(paras)["root_dir"] = "../data" 
        vars(paras)["output_dir"] = "output/"
        print(" I shouldnt be working local! There is no data here")
    else:
        vars(paras)["root_dir"] = "/projects/p_da_aipp/DieckmannJakob/LWFAdata"
        vars(paras)["output_dir"] = "/scratch/ws/1/s5693273-output/Shellnet/LWFA/"
        # vars(paras)["output_dir"] = "/scratch/ws/1/s5693273-output/Shellnet/LWFA/"
    
    mytime = time.strftime("%d-%m_%H:%M:%S")
    vars(paras)["output_dir"] = paras.output_dir + mytime  
    print("this is the output ", paras.output_dir)

    

    
    
    return paras
    

def start_train(parser):
    
        
    """
    This function initializes the training as a wandb run. 
    
    More detail in train()
    """
    
    paras = get_paras(parser)
    
    #Setup Weights and Biases / Horovod
    entity = "annaandme" if paras.in_team else "jakobdieckmann"   
    name = "LWFA_Shell_local" if paras.work_local else "LWFA_Shell_taurus"          
    
    hvd = None
    output_node = True # if (not paras.use_horovod or hvd.rank() == 0) else False
    vars(paras)["output_node"] = output_node
    
    print("ausgabe, wandb", paras.output_node, paras.wandb)
    if paras.output_node and paras.wandb:
        print("wandb should be initialized here")
        run = wandb.init(entity=entity, project=name, config=paras) 
    else:
       run = None
    
    train(run, paras, hvd)
    
def train(run, paras, hvd):
        
    """
    This function trains a model and plots and saves the results. 

    Arguments:
        - Model Parameters
        - Training Parameters
        - Data Parameters
        - Environment Parameters
    
    
    """
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("device: ", device)
    vars(paras)["device"] = device
    # num_points needs to be square number
    paras.num_points = math.floor(math.sqrt(paras.num_points))**2
    print("The number of points was corrected to: ", paras.num_points)

    
    # Import data
    my_dataset = Dataset(paras)   
    
    # Dataloader      
    train_loader, test_loader = create_dataloaders(my_dataset, paras)
    
    print("loaders loaded")
    plot_data(train_loader, paras)
    
    model = ShellAuto(paras)
    model = model.to(device)
    
    if paras.binned_loss:
        criterion = Binned_mse(paras)
    else:
        criterion = nn.MSELoss(reduction="mean")
    vars(paras)["criterion"] = criterion

    optimizer = torch.optim.Adam(model.parameters(), lr=paras.learning_rate)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=paras.gamma)
    
    if paras.output_node and paras.wandb:
        wandb.watch(model, criterion, log="all", log_freq=5)
    
    min_train_loss = 10000000
    min_test_loss = 10000000
    
    for epoch in range(1, paras.n_epochs+1):
        print("start new epoch")
        # monitor training loss
        train_loss = 0.0
        test_loss = 0.0
        
        model.eval()
        for batch in test_loader:
            pointclouds = batch[0].type(torch.FloatTensor).cuda()
            outputs = model(pointclouds)
            loss = criterion(outputs, pointclouds)
            test_loss += loss.item() # pointclouds.size(0) = batchsize
            if epoch == paras.n_epochs:
                bin_test_loss = criterion(bin_pcs(outputs), bin_pcs(pointclouds))
            
        # Training
        model.train()
        for batch in train_loader:
            pointclouds = batch[0].type(torch.FloatTensor).cuda()
            optimizer.zero_grad()
            outputs = model(pointclouds)
            loss = criterion(outputs, pointclouds)
            loss.backward()
            optimizer.step()
            train_loss += loss.item() # *pointclouds.size(0) = batchsize
            if epoch == paras.n_epochs:
                bin_train_loss = criterion(bin_pcs(outputs), bin_pcs(pointclouds))
        scheduler.step()
                    
        test_loss = test_loss/len(test_loader)    
        train_loss = train_loss/len(train_loader)     
        if min_train_loss > train_loss:
            min_train_loss = train_loss
        if min_test_loss > test_loss:
            min_test_loss = test_loss
        
        # Log and Print results of Epoch
        if paras.output_node:
            print('Epoch: {} \tTraining Loss: {:.6f} \tTest Loss: {:.6f}'.format(epoch, train_loss, test_loss))
            if paras.wandb:
                run.log({"training_loss": train_loss,
                   "test_loss": test_loss,
                   "min_train_loss": min_train_loss,
                   "min_test_loss": min_test_loss,
                   "latent_dim": paras.latent_dim}, step=epoch)
         
        # # Checkpoint
        # if paras.output_node and (epoch%50 == 0 or epoch == paras.n_epochs):
        #     checkpoint(model, paras)

    model.eval()     
    # Plot Results
    if paras.output_node:  
        print("plotting and saving")
        plot_results(model, train_loader, test_loader, paras)
        save_results(model, test_loader, paras, run)
        if paras.wandb:
            bin_test_loss /= len(test_loader)    
            bin_train_loss /= len(train_loader) 
            
            print("bin_train_loss: ",  bin_train_loss,
                   "bin_test_loss :", bin_test_loss)
            wandb.finish()
        
    
def checkpoint(model, paras):
    mytime = time.strftime("%d-%m-%Y_%H:%M")
    modelfile = paras.output_dir + mytime + "_model.pt"
    Path(os.path.dirname(modelfile)).mkdir(parents=True, exist_ok=True)
    torch.save(model.state_dict(), modelfile)
    # Path(os.path.dirname(check_file)).mkdir(parents=True, exist_ok=True)
    # checkpoint = dict(wandb.config)
    # checkpoint.update({"optimizer_state": optimizer.state_dict(),
    #                         "model_state": model.state_dict(),
    #                         "epoch": epoch})
    # save_checkpoint(checkpoint, run, check_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    start_train(parser)
    
    print("program should be nicely finished")
