#!/bin/bash -l
#SBATCH --job-name sweep_Shell   # name of the job
#SBATCH -p ml
#SBATCH --output /scratch/ws/1/s5693273-output/logfiles/%j.out
#SBATCH --error /scratch/ws/1/s5693273-output/logfiles/%j.err
#SBATCH -A p_da_aipp
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=15 # should be equal to number of threads
#SBATCH --time 70:59:00       # walltime (up to 96 hours)
#SBATCH --gres gpu:1
#SBATCH --mail-user jakob.dieckmann@tu-dresden.de
#SBATCH --mail-type ALL
#SBATCH --reservation p_da_aipp_463

module load modenv/ml
module load CMake/3.15.3-GCCcore-8.3.0
module load PyTorch/1.6.0-fosscuda-2019b-Python-3.7.4
module load matplotlib/3.1.1-fosscuda-2019b-Python-3.7.4
module load h5py/2.10.0-fosscuda-2019b-Python-3.7.4

source ~/myhorovod/bin/activate
wandb agent --count 7 jakobdieckmann/LWFA_Shell_taurus/30lo7m4r





