#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 09:34:14 2021

@author: backdirneis
"""

import torch
import numpy as np
# if use_horovod:
    # import horovod.torch as hvd

# def create_dataloaders(dataset, parameters):
    
#     train_data, test_data = train_test_split(dataset)

    
#     # Limit data for faster execution
#     if parameters.fast_run:
#         train_data = torch.utils.data.Subset(train_data, range(20))
#         test_data = torch.utils.data.Subset(test_data, range(20))
        
        
#     # Partition dataset among workers using DistributedSampler
#     if use_horovod:
#         train_sampler = torch.utils.data.distributed.DistributedSampler(
#                             train_data, num_replicas=hvd.size(), rank=hvd.rank())
#         test_sampler = torch.utils.data.distributed.DistributedSampler(
#                             test_data, num_replicas=hvd.size(), rank=hvd.rank())
#     else:
#         train_sampler = None
#         test_sampler = None
            
    
#     # Prepare data loaders
#     train_loader = torch.utils.data.DataLoader(train_data, batch_size=parameters.batchsize, num_workers=0, sampler=train_sampler)
#     test_loader = torch.utils.data.DataLoader(test_data, batch_size=parameters.batchsize, num_workers=0, sampler=test_sampler)
    
#     return (train_loader, test_loader)

def create_dataloaders(dataset, parameters):
    
    train_data, test_data = train_test_split(dataset)

    
    # Limit data for faster execution
    if parameters.fast_run:
        train_data = torch.utils.data.Subset(train_data, range(20))
        test_data = torch.utils.data.Subset(test_data, range(20))
            
    
    # Prepare data loaders
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=parameters.batchsize, drop_last=True, num_workers=0, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=parameters.batchsize, drop_last=True, num_workers=0, shuffle=True)
    
    return (train_loader, test_loader)



def train_test_split(dataset):
    """
    This function splits dataset into Test and Training set. The question of how to divide is not 
    easy to answer. Thomas Mietlinger gave a forumale by which I could sort the 
    simulations, but this is not proven by anything. 

    Parameters
    ----------
    dataset : 

    Returns
    -------
    train_data : Subset with train data
    test_data : Subset with test data

    """
    
    percentage = 20

    # order = np.zeros((len(dataset), 2))
    # order[0] = [1, 2]
    
    # for i in range(len(dataset)):
    #     values = dataset[i][1]
    #     values = list(map(float, values))
    #     # Critirion for odering elements (a0^2 * tau) / (n0 * d)
    #     critirion = values[0]**2 * values[1] / (values[2] * values[3])
    #     order[i] = [int(i),  critirion] # [index, criterion]
    
    # ordered = order[order[:,1].argsort()] # Sort array by critirion
        
    # last_ele = 0
    # every = 0
    # use_as_test = False
    # test_indice = []
        
    # ### put elements in test as following: (every 4th := 25%)
    # # 12 12 12 14 14 17 17 17 17 21 21 21 34 34 34 56 56 64 72 72 72 83 83  
    # #                            < test >                   < test >
    
    # i_th = int(100 / percentage) 
    
    # for date in ordered:
    #     if date[1] != last_ele:
    #         # change of parameters happening
    #         last_ele = date[1]
    #         every += 1
    #         if every % i_th == 0:
    #             use_as_test = True # every i_th change dates should be used in test set
    #         else:
    #             use_as_test = False
        
    #     if use_as_test:
    #         test_indice.append(int(date[0])) # the index is used for test set
    
    # train_indice = list(range(len(dataset)))
    # train_indice = [e for e in train_indice if e not in test_indice]
    # test_data = torch.utils.data.Subset(dataset, test_indice)
    # train_data = torch.utils.data.Subset(dataset, train_indice) 
    
    train_size = int(len(dataset)*(1-percentage/100))
    train_data, test_data = torch.utils.data.random_split(dataset, [train_size, len(dataset) - train_size], torch.Generator())
    
    return train_data, test_data

