#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:45:44 2021

@author: backdirneis
"""

import torch.nn as nn
import numpy as np
import torch
import pdb
from argparse import Namespace
from helpfunctions import random_sample

class Unflatten(nn.Module):
    """ 
    It should function as a reverse nn.Flatten Module.
    Has the option of skip connection.
    
    Args:
        start_dim: Array is unflattened in dimension >= start_dim
        
        shape: Shape of output Tensor. torch.prod(shape) needs to be equal 
                to input tensor shape. 
    """
    
    
    def __init__(self, start_dim, shape):
        super(Unflatten, self).__init__()
        self.shape = shape
        self.start_dim = start_dim

    def forward(self, x, skip_feat=None):
        x = x.view((tuple(x.shape[0:self.start_dim],)) + self.shape)
        x = torch.cat((x, skip_feat), dim=-1) if skip_feat is not None else x
        return x

def knn(points, queries, K):
    """
    Args:
        points ( B x N x 2 tensor )
        query  ( B x M x 2 tensor )  M < N 
        K      (constant) num of neighbors
    Outputs:
        knn    (B x M x K x 2 tensor) sorted K nearest neighbor
        indice (B x M x K tensor) knn indices   
    """
    value = None
    indices = None
    num_batch = points.shape[0]
    for i in range(num_batch):
        point = points[i]
        query = queries[i]
        dist  = torch.cdist(point, query) 
        idxs  = dist.topk(K, dim=0, largest=False, sorted=True).indices
        idxs  = idxs.transpose(0,1)
        nn    = point[idxs].unsqueeze(0)
        value = nn if value is None else torch.cat((value, nn))

        idxs  = idxs.unsqueeze(0)
        indices = idxs if indices is None else torch.cat((indices, idxs))
        
    return value, indices.long()

def gather_feature(features, indices):
    """
    Args:
        features ( B x N x F tensor) -- feature from previous layer
        indices  ( B x M x K tensor) --  represents queries' k nearest neighbor
    Output:
        features ( B x M x K x F tensor) -- knn features from previous layer 
    """
    res = None
    num_batch = features.shape[0]
    for B in range(num_batch):
        knn_features = features[B][indices[B]].unsqueeze(0)
        res = knn_features if res is None else torch.cat((res, knn_features))
    return res

class Dense(nn.Module):
    def __init__(self, in_size, out_size, in_dim=3, 
            has_bn=True, drop_out=None):
        super(Dense, self).__init__()   
        """
        I would argue that the following explanation by the author is not correct. 
        Args:
            input ( B x M x K x 3  tensor ) -- subtraction vectors 
                from query to its k nearest neighbor
        Output: 
            local point feature ( B x M x K x 64 tensor ) 
        """
        self.has_bn = has_bn
        self.in_dim = in_dim

        if in_dim == 3 or in_dim == 2:
            self.batchnorm = nn.BatchNorm1d(in_size)
        elif in_dim == 4:
            self.batchnorm = nn.BatchNorm2d(in_size)
        else: 
            self.batchnorm = None

        if drop_out is None:
            self.linear = nn.Sequential(
                nn.Linear(in_size, out_size),
                nn.LeakyReLU()
            )
        else:
            self.linear = nn.Sequential(
                nn.Linear(in_size, out_size),
                nn.LeakyReLU(),
                nn.Dropout(drop_out)
            )            

    def forward(self, inputs):

        if self.has_bn == True:
            d =  self.in_dim - 1
            outputs = self.batchnorm(inputs.transpose(1, d)).transpose(1, d)
            outputs = self.linear(outputs)
            return outputs

        else:
            outputs = self.linear(inputs)
            return outputs

class ShellConv(nn.Module):
    def __init__(self, out_features, prev_features, neighbor, division, 
            has_bn=True):
        super(ShellConv, self).__init__() 
        """
        out_features  (int) num of output feature (dim = -1)
        prev_features (int) num of prev feature (dim = -1)
        neighbor      (int) num of nearest neighbor in knn
        division      (int) num of division
        """

        self.K = neighbor
        self.S = int(self.K/division) # num of feature per shell
        self.F = 64 # num of local point features
        # self.neighbor = neighbor
        in_channel = self.F + prev_features
        out_channel = out_features

        self.dense1 = Dense(2, self.F // 2, in_dim=4, has_bn=has_bn) # first number needs to adapt to dimension!!!
        self.dense2 = Dense(self.F // 2, self.F, in_dim=4, has_bn=has_bn)
        self.maxpool = nn.MaxPool2d((1, self.S), stride = (1, self.S))
        if has_bn == True:
            self.conv = nn.Sequential(
                nn.BatchNorm2d(in_channel),
                nn.Conv2d(in_channel, out_channel, (1, division)),
                nn.LeakyReLU(),
            )
        else:
            self.conv = nn.Sequential(
                nn.Conv2d(in_channel, out_channel, (1, division)),
                nn.LeakyReLU(),
            )
        
    def forward(self, points, queries, prev_features):
        """
        Args:
            points          (B x N x 3 tensor)
            query           (B x M x 3 tensor) -- note that M < N
            prev_features   (B x N x F1 tensor)
        Outputs:
            feat            (B x M x F2 tensor)
        """

        knn_pts, idxs = knn(points, queries, self.K)
        knn_center    = queries.unsqueeze(2)
        knn_points_local = knn_center - knn_pts

        knn_feat_local = self.dense1(knn_points_local)
        knn_feat_local = self.dense2(knn_feat_local)

        # shape: B x M x K x F
        if prev_features is not None:
            knn_feat_prev = gather_feature(prev_features, idxs)
            knn_feat_cat  = torch.cat((knn_feat_local, knn_feat_prev), dim=-1)
        else:
            knn_feat_cat  = knn_feat_local 

        knn_feat_cat = knn_feat_cat.permute(0,3,1,2) # BMKF -> BFMK        
        knn_feat_max = self.maxpool(knn_feat_cat)
        output   = self.conv(knn_feat_max).permute(0,2,3,1)

        return output.squeeze(2)

class ShellUp(nn.Module):
    def __init__(self, out_features, prev_features, neighbor, division, 
            has_bn=True):
        super(ShellUp, self).__init__()
        self.has_bn = has_bn
        self.sconv = ShellConv(out_features, prev_features, neighbor,
            division, has_bn)
        self.dense = Dense(2 * out_features, out_features, has_bn=has_bn)

    def forward(self, points, queries, prev_features, feat_skip_connect):
        sconv = self.sconv(points, queries, prev_features)
        feat_cat = torch.cat((sconv, feat_skip_connect), dim=-1)

        outputs = self.dense(feat_cat)
        return outputs
    
    
class ShellAuto(nn.Module):
    """
    This class contains an Autoencoder based on the shellnet Library kindly provided by
    Dung-Han_lee. 
    https://github.com/Dung-Han-Lee/Pointcloud-based-Row-Detection-using-ShellNet-and-PyTorch
    
    Roughly following the Convolutional Architecture of Lee and Carlberg.
    
    Args:
        c_filters:      Number of filters in convolutional layers
        num_shells:     Number of Shells in covolutional layers          
        neighbours:     Number of neighbours for knn calculation
        start_query, points_query:
                            Reducing number of points in convolutional layers 
                        starting from start_query and then reducing in points_query
        d_filters:      Number of features in Dense Layers = Number of points * Features (flattened)

    
    """
    def __init__(self, paras):
        super(ShellAuto, self).__init__()
        has_bn = paras.batchnorm        

        self.points_query = [int(paras.start_query / (2**j)) for j in range(paras.num_downsampling)]
        c_filters = [int(paras.c_filters / (2**j)) for j in range(paras.num_downsampling)][::-1]
        num_shells = [2**j for j in range(paras.num_downsampling)][::-1]
        neighbors = [8 * (2**j) for j in range(paras.num_downsampling)][::-1]

        before = (self.points_query[-1], c_filters[-1])
        
        d_filters = [ int(paras.latent_size * (2**j)) for j in range(paras.linear_layers, -1, -1) ]
        d_filters[0] = int(np.prod(before))
        
        
        before = (self.points_query[-1], c_filters[-1])
        
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.shellconv1 = ShellConv(c_filters[0],   0, neighbors[0], num_shells[0], has_bn)
        self.shellconv2 = ShellConv(c_filters[1], c_filters[0], neighbors[1], num_shells[1], has_bn)
        self.shellconv3 = ShellConv(c_filters[2], c_filters[1],  neighbors[2], num_shells[2], has_bn) 
        
        self.flatten = nn.Flatten(start_dim=1, end_dim=-1)
        

        self.fc1 = Dense(d_filters[0], d_filters[1], has_bn=has_bn, in_dim=2, drop_out=0)
        self.fc2 = Dense(d_filters[1], d_filters[2], has_bn=has_bn, in_dim=2, drop_out=0)
        self.fc3 = Dense(d_filters[2],   d_filters[3], in_dim=2, has_bn=has_bn)
        
        self.fcu3 = Dense(d_filters[3], d_filters[2], has_bn=has_bn, in_dim=2, drop_out=0)
        self.fcu2 = Dense(d_filters[2], d_filters[1], has_bn=has_bn, in_dim=2, drop_out=0)
        self.fcu1 = Dense(d_filters[1],   d_filters[0], in_dim=2, has_bn=has_bn)
        
        self.unflatten = Unflatten(1, before)
        
        self.shellup3   = ShellUp(  c_filters[1], c_filters[2],  neighbors[2], num_shells[2], has_bn)
        self.shellup2   = ShellUp(  c_filters[0], c_filters[1], neighbors[1], num_shells[1], has_bn)
        self.shellup1   = ShellConv(2, c_filters[0], neighbors[0], num_shells[0], has_bn)


    def forward(self, inputs):
        
        import GPUtil
        
        print("inputs", inputs.shape, inputs)
        
        print("Here memory should be almost free: ", GPUtil.getGPUs()[0].memoryUsed)
        
        # print("inputs.shape, memory use ", inputs.shape)
        use = GPUtil.getGPUs()[0].memoryUsed
        use2 = GPUtil.getGPUs()[0].memoryUsed
        query1 = random_sample(inputs, self.points_query[0])
        sconv1 = self.shellconv1(inputs, query1, None)
        print("sconv1.shape, memory use  ", sconv1.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("sconv1", sconv1)
        
        query2 = random_sample(query1, self.points_query[1])
        sconv2 = self.shellconv2(query1, query2, sconv1)
        print("sconv2.shape, memory use ", sconv2.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("sconv2", sconv2)

        query3 = random_sample(query2, self.points_query[2])
        sconv3 = self.shellconv3(query2, query3, sconv2)
        print("sconv3.shape, memory use ", sconv3.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("sconv3", sconv3)

   
        
        
        flat = self.flatten(sconv3)
        print("flat.shape, memory use  ", flat.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        

        
        
        fc1 = self.fc1(flat)
        # print("fc1.shape, memory use  ", fc1.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed


        fc2 = self.fc2(fc1)
        # print("fc2.shape, memory use ", fc2.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed


        latent = self.fc3(fc2)
        # print("latent vector shape, memory use  ", latent.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        # print("latent", latent.shape, latent)

        
        
        fcu3 = self.fcu3(latent)
        # print("fcu3.shape , memory use ", fcu3.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed


        fcu2 = self.fcu2(fcu3)
        # print("fcu2.shape, memory use ", fcu2.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed


        fcu1 = self.fcu1(fcu2)
        # print("fcu1.shape, memory use ", fcu1.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("fcu1", fcu1.shape, fcu1)

        
        
        unflat = self.unflatten(fcu1, None) # Here I could add a skip connection, 
        # print("unflat.shape, memory use ", unflat.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed

        
        
        up3    = self.shellup3(query3, query2, unflat, sconv2)
        # print("up3.shape, memory use ", up3.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("up3", up3.shape, up3)


        up2    = self.shellup2(query2, query1, up3, sconv1)
        # print("up2.shape, memory use  ", up2.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        # use = GPUtil.getGPUs()[0].memoryUsed
        print("up2", up2.shape, up2)
        
        print("all but last layer use %2.1f MB " %(GPUtil.getGPUs()[0].memoryUsed - use2))

        reconstructed    = self.shellup1(query1, inputs, up2)
        print("reconstructed.shape, memory use ", reconstructed.shape, GPUtil.getGPUs()[0].memoryUsed - use)
        print("reconstructed", reconstructed)
        torch.cuda.empty_cache()

        return reconstructed



if __name__ == '__main__':
    """
    Test model function
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    paras = Namespace()
    vars(paras)["num_points"] = 4000
    vars(paras)["batchnorm"] = True
    vars(paras)["linear_layers"] = 3
    vars(paras)["latent_size"] = 150  
    vars(paras)["start_query"] = 512
    vars(paras)["c_filters"] = 8
    vars(paras)["num_downsampling"] = 3
    B = 4
    M = paras.num_points
    K = 32

    # Create random Tensors to hold inputs and outputs
    p = torch.randn(B, M, 2)
    q = torch.randn(B, M//10, 2)
    #f = torch.randn(B, M, 128)
    #y = torch.randn(B, M//2, 128)


    nn_pts, idxs = knn(p, q, K)
    nn_center    = q.unsqueeze(2)
    nn_points_local = nn_center - nn_pts

    #model = ShellNet(2, 1024, conv_scale=1, dense_scale=1)
    model = ShellAuto(paras)
    
    p_cuda = p.to(device)
    model = model.to(device)
    print("model on cuda?", next(model.parameters()).is_cuda)
    output = model(p_cuda)
    #print(model(p_cuda).detach().cpu().shape)
    
    del model
    del p
    del p_cuda
    del nn_pts, idxs, nn_center, q, B, M, K
    torch.cuda.empty_cache()

