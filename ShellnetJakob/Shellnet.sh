#!/bin/bash -l
#SBATCH --job-name ShellJak   # name of the job
#SBATCH -p ml
#SBATCH --output logfiles/%j.out
#SBATCH --error logfiles/%j.err
#SBATCH -A p_da_aipp
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=29 # should be equal to number of threads
#SBATCH --time 43:59:00       # walltime (up to 96 hours)
#SBATCH --gres gpu:1
#SBATCH --mem-per-cpu 1000
#SBATCH --mail-user jakob.dieckmann@tu-dresden.de
#SBATCH --mail-type ALL

module load modenv/ml
module load PyTorch/1.6.0-fosscuda-2019b-Python-3.7.4
module load matplotlib/3.1.1-fosscuda-2019b-Python-3.7.4
module load h5py/2.10.0-fosscuda-2019b-Python-3.7.4
module load CMake/3.15.3-GCCcore-8.3.0

source ~/myhorovod/bin/activate
srun python3 train.py --num_points 4096 --latent_size 256 --n_epochs 200 --wandb --learning_rate 0.01 --gamma 0.9 --taurus --c_filters 64
