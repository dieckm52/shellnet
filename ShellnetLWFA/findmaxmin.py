#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 17:27:48 2021

@author: backdirneis
"""


import wandb
import torch
import time
import torch.nn as nn
import math
import os.path
import argparse
from pathlib import Path
from model import ShellAuto
from dataset import Dataset
from dataloader import create_dataloaders
from plotandsave import plot_results, save_results, plot_data

def get_paras(parser):
    """
    Get all the user input parameters

    Parameters
    ----------
    parser : Parser

    Returns
    -------
    paras : Namespace Object with all the parameters

    """
    
    parser.add_argument('--batchsize', type=int, default=4)
    parser.add_argument('--first_stamp', type=int, default=0)
    parser.add_argument('--number_per_file', type=int, default=10)
    parser.add_argument('--num_points', type=int, default=2048)
    parser.add_argument('--learning_rate', type=float, default=0.01)
    parser.add_argument('--gamma', type=float, default=1.0)
    parser.add_argument('--num_downsampling', type=int, default=3)
    parser.add_argument('--num_conv_samesize', type=int, default=0)
    parser.add_argument('--c_filters', type=int, default=512)
    parser.add_argument('--start_query', type=int, default=512)    
    parser.add_argument('--latent_size', type=int, default=64)
    parser.add_argument('--linear_layers', type=int, default=3)
    parser.add_argument('--no-batchnorm', dest='batchnorm', action='store_false')
    parser.add_argument('--n_epochs', type=int, default=1)
    parser.add_argument('--no-normalize', dest='normalize', action='store_false')
    parser.add_argument('--taurus', dest='work_local', action='store_false')
    parser.add_argument('--fast_run', dest='fast_run', action='store_true')
    parser.add_argument('--in_team', dest='in_team', action='store_true')
    parser.add_argument('--wandb', dest='wandb', action='store_true')
    parser.add_argument('--no-skip_connect', dest='skip_connect', action='store_false')
    parser.add_argument('--start_from_check', dest='start_from_check', action='store_true')
    paras = parser.parse_args()
    
    if paras.work_local:
        vars(paras)["root_dir"] = "../data" 
        vars(paras)["output_dir"] = "output/"
        print(" I shouldnt be working local! There is no data here")
    else:
        vars(paras)["root_dir"] = "/projects/p_da_aipp/DieckmannJakob/LWFAdata"
        vars(paras)["output_dir"] = "/scratch/ws/1/s5693273-output/Shellnet/LWFA/"
    
    
    mytime = time.strftime("%d-%m-%Y_%H:%M")
    vars(paras)["output_dir"] = paras.output_dir + mytime  
    # num_points needs to be square number
    paras.num_points = math.floor(math.sqrt(paras.num_points))**2
    print("The number of points was corrected to: ", paras.num_points)
    
    
    return paras
    

def start_train(parser):
    
        
    """
    This function initializes the training as a wandb run. 
    
    More detail in train()
    """
    
    paras = get_paras(parser)
    
    #Setup Weights and Biases / Horovod 
    entity = "annaandme" if paras.in_team else "jakobdieckmann"   
    name = "ShellnetLocal" if paras.work_local else "ShellnetTaurus"          
    
    hvd = None
    output_node = True # if (not paras.use_horovod or hvd.rank() == 0) else False
    vars(paras)["output_node"] = output_node
    
    print("ausgabe, wandb", paras.output_node, paras.wandb)
    if paras.output_node and paras.wandb:
        print("wandb should be initialized here")
        run = wandb.init(entity=entity, project=name, config=paras) 
    else:
       run = None
    
    train(run, paras, hvd)
    
def train(run, paras, hvd):
        
    """
    This function trains a model and plots and saves the results. 

    Arguments:
        - Model Parameters
        - Training Parameters
        - Data Parameters
        - Environment Parameters
    
    
    """
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("device: ", device)
    vars(paras)["device"] = device

    
    # Import data
    my_dataset = Dataset(paras)   
    
    # Dataloader      
    train_loader, test_loader = create_dataloaders(my_dataset, paras)
    
    print("loaders loaded")
   
    maxi = -100
    mini = 100 
    maxy = -100
    miny = 100 
    
    for batch in train_loader:
        thismax = torch.max(batch[0][:,:,0])
        thismin = torch.min(batch[0][:,:,0])
        thismay = torch.max(batch[0][:,:,1])
        thismiy = torch.min(batch[0][:,:,1])
        
        maxi = maxi if thismax < maxi else thismax
        maxy = maxy if thismay < maxy else thismay
        
        mini = mini if thismin > mini else thismin
        miny = miny if thismiy > miny else thismiy
        
        
    for batch in train_loader:
        thismax = torch.max(batch[0][:,:,0])
        thismin = torch.min(batch[0][:,:,0])
        thismay = torch.max(batch[0][:,:,1])
        thismiy = torch.min(batch[0][:,:,1])
        
        maxi = maxi if thismax < maxi else thismax
        maxy = maxy if thismay < maxy else thismay
        
        mini = mini if thismin > mini else thismin
        miny = miny if thismiy > miny else thismiy
        
        
    print("max min", maxi, mini) 
    print("may miy", maxy, miny)    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    start_train(parser)
    
    print("program should be nicely finished")