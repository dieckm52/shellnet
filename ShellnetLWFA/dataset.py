#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 12:40:27 2021

@author: backdirneis
"""

import os
from os.path import isfile, join
import h5py
import torch
import re
import numpy as np
import openpmd_api as io 
from torch.utils.data import Dataset
from helpfunctions import random_sample, shell_sample


def load_data(filename):
    series = io.Series(filename, io.Access.read_only)
    dataindex = next(iter(series.iterations))
    data = series.iterations[dataindex]
    electron_data = data.particles["e"]
    position_data = electron_data["position"]
    offset_data = electron_data["positionOffset"]
    momentum_data = electron_data["momentum"]
    series.flush()
    
    position_y_data = position_data["y"]
    offset_y_data = offset_data["y"]
    momentum_y_data = momentum_data["y"]
    series.flush()
    
    position_y = position_y_data.load_chunk()
    offset_y = offset_y_data.load_chunk()
    momentum_y = momentum_y_data.load_chunk()
    series.flush()
    

        
    
    return position_y + offset_y, momentum_y

    
    


class Dataset(Dataset):
    def __init__(self, paras, transform=None):
                
        self.parameters = paras
        self.transform = transform

    def __len__(self):
        sdr = os.listdir(self.parameters.root_dir)
        len_dataset = len(sdr) * self.parameters.number_per_file
        return len_dataset 
    
    def __getitem__(self, idx):
        
        """
        Provides single data elements for the Grid Approach function

        Parameters
        ----------
        idx : Iterator for all elements

        Returns
        -------
        image : The data that can be treated as image due to its two dimensions
        label : Contains information about parameters of simulation

        """
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
     
        ## Data has the following structure:
             
         # root_dir
             # cdr1
                 # fsd1
                     # ssd1
             # cdr2 
                 # fsd2
                     # ssd2
             # cdr3
             # .
             # .
             # .
              # cdr 1367
         
        sdr = os.listdir(self.parameters.root_dir) # folder with data
        stringname = sdr[idx // self.parameters.number_per_file]
        cdr = os.path.join(self.parameters.root_dir, stringname) #subfolder_structure
        dirname = os.path.join(cdr, "simOutput/openPMD/") #subfolder_structure
     
        ts = self.parameters.first_stamp + (idx % self.parameters.number_per_file) # every timestamp is a new date;
        ts_string = "{:02d}".format(ts)
        dataname = "simData_0" + ts_string + "000.bp"
        
        
        #print("timestamp im durchlauf ist " + str(ts))
        
        position, momentum = load_data(os.path.join(dirname, dataname))
        
        if self.parameters.normalize: # Normalize
            position = position / 3200
            momentum = (momentum + 0.5) / 1.5
            
        pointcloud = np.stack((position, momentum), 0)

        pointcloud = torch.tensor(pointcloud, requires_grad=True).unsqueeze(0).transpose(1, 2)
        pointcloud = shell_sample(pointcloud, self.parameters.num_points).squeeze()
        # pointcloud = random_sample(pointcloud, self.parameters.num_points).squeeze()
        pointcloud = pointcloud.to(device).float()
        
        if self.parameters.easy_pcs:
            pointcloud[:, 1] = 0.5
        
        
        labels = "run_" + stringname[:3] + "_ts_" + ts_string
        
        return [pointcloud, labels]
        