#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 11:05:30 2021

@author: backdirneis
"""
import torch
import math
import wandb
from copy import deepcopy

# Define Image show function
def imshow(pc, ax, color=None, ):
    pc = pc.cpu()
    ax.scatter(pc[:, 0], pc[:, 1], linewidth=0.5, s=0.2, c=color)
    ax.set_xlabel("position x")
    ax.set_ylabel("momentum px")
    
def create_labelstring(labels):
    label = "a0-" + labels[0]
    label += "_tau-" + labels[1]
    label += "_n0-" + labels[2]
    label += "_d-" + labels[3]
    label += "_L0-" + labels[4]
    label += "_L1-" + labels[5]
    label += "_ts-" + labels[6]
    return label

def save_checkpoint(checkpoint, run, check_file):
    metadata = deepcopy(checkpoint)
    metadata.pop("model_state")
    metadata.pop("optimizer_state")
    torch.save(checkpoint, check_file)
    artifact= wandb.Artifact(name='checkpoint', type='checkpoint', metadata=metadata)
    artifact.add_file(check_file, name="checkpoint_data")
    torch.save(checkpoint, check_file)
    run.log_artifact(artifact)
    artifact.wait()
    
def load_checkpoint(run, version="latest"):
    artifact = run.use_artifact('checkpoint:' + version)
    artifact_dir = artifact.download()
    data = torch.load(artifact_dir + "/latest.tar") 
    model_state = data["model_state"]
    optim_state = data["optimizer_state"]
    return model_state, optim_state

def get_chunk_list(total_num, num_x_shells):
    """
    The total Number (total_num) of elements should be split into num_x_shellss 
    equally sized chunks. Difficulty is that total_num may not be devisble by 
    num_x_shells.  
    """
    
    lower_size = math.floor(total_num / num_x_shells)
    size_list = [lower_size] * num_x_shells
    higher_size = total_num - (lower_size * num_x_shells)
    size_list[:higher_size] = map(lambda x: x+1, size_list[:higher_size])
    return size_list

def random_sample(points, num_sample):
    """
    Args:
        points ( B x N x 2 tensor )
        num_sample (constant)
    Outputs:
        sampled_points (B x num_sample x 2 tensor)
    """    
    perm = torch.randperm(points.shape[1])
    return points[:, perm[:num_sample]].clone()

def shell_sample(points, num_sample):
    """
    Mean Pooling within shells in the input pointcloud. Purpose is a
    preprocessing step that reduces the number of points in the cloud, while 
    conserving as much information as possible. Alternative to random_sample. 
    
    Algorithm:
        For every Pointcloud in batch:
        1. Devide x space into xc = root(num_sample) spaces with k = constant
        2. Divide y space into yc = num_sample/xc spaces with k = constant
        3. Calculate mean in every "shell"
    
    Args:
        points ( B x N x 2 tensor )
        num_sample (constant)
    Outputs:
        sampled_points (B x num_sample x 2 tensor)
    """  
    sampled_points = torch.tensor([])
        
    num_x_shells = int(math.sqrt(num_sample))
    num_y_shells = num_sample // num_x_shells
    num_x_points = get_chunk_list(points.shape[1], num_x_shells)

    
    # 1. 
    for pc in points:
        xsort = pc[:, 0].sort()[1]
        pc = pc[xsort]
        x_shells = torch.split(pc, num_x_points, dim=0)
        # 2. 
        batchsamples = []
        for x_shell in x_shells:
            ysort = x_shell[:, 1].sort()[1] # indices
            x_shell = x_shell[ysort]
            num_y_points = get_chunk_list(ysort.shape[0], num_y_shells)
            y_shells = torch.split(x_shell, num_y_points, dim=0)
            # 2. and 3.
            batchsamples += [torch.mean(y_shell, 0) for y_shell in y_shells]
        batchsamples = torch.stack(batchsamples).unsqueeze(0)
        sampled_points = torch.cat((sampled_points, batchsamples), 0) if sampled_points.shape[0] else batchsamples
    
    return random_sample(sampled_points, num_sample)
            
def test_shell_sample():
    # import pdb; pdb.set_trace()

    inTens = torch.FloatTensor([[[0, 2],[-1, -3],[-5, 4],[2, 7],[-3, 4],[2, 4],[5, -2],[1, 7]], [[1, 4],[-1, -3],[-5, 4],[2, 7],[-3, 4],[2, 4],[5, -2],[1, 7]]])
    outTens = shell_sample(inTens, 4)   
    exact = torch.tensor([[[ 1.5000,  7.0000],
         [-4.0000,  4.0000],
         [-0.5000, -0.5000],
         [ 3.5000,  1.0000]],

        [[ 1.5000,  7.0000],
         [-1.0000,  4.0000],
         [-3.0000,  0.5000],
         [ 3.5000,  1.0000]]])
    print("all should be True!", torch.max(exact) == torch.max(outTens), torch.min(exact) == torch.min(outTens), torch.mean(exact) == torch.mean(outTens), torch.var(exact) == torch.var(outTens))
            


