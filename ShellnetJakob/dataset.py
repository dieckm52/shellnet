#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 12:40:27 2021

@author: backdirneis
"""

import os
from os.path import isfile, join
import h5py
import torch
import re
import numpy as np
from torch.utils.data import Dataset
from helpfunctions import random_sample, shell_sample


class Dataset(Dataset):
    def __init__(self, paras, transform=None):
                
        self.parameters = paras
        self.transform = transform

    def __len__(self):
        sdr = os.listdir(self.parameters.root_dir)
        len_dataset = len(sdr) * self.parameters.number_per_file
        return len_dataset 
    
    def __getitem__(self, idx):
        
        """
        Provides single data elements for the Grid Approach function

        Parameters
        ----------
        idx : Iterator for all elements

        Returns
        -------
        image : The data that can be treated as image due to its two dimensions
        label : Contains information about parameters of simulation

        """
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
     
        ## Data has the following structure:
             
         # root_dir
             # cdr1
                 # fsd1
                     # ssd1
             # cdr2 
                 # fsd2
                     # ssd2
             # cdr3
             # .
             # .
             # .
              # cdr 1367
         
        sdr = os.listdir(self.parameters.root_dir) # folder with data
        cdr = os.path.join(self.parameters.root_dir, sdr[idx // self.parameters.number_per_file]) #subfolder_structure
        fsd = os.path.join(cdr, "402c_rand_relaxation_phasespace") #subfolder_structure
        ssd = os.path.join(fsd, os.listdir(fsd)[0]) #subfolder_structure
        
        path = os.path.join(ssd, "TrackParticlesDisordered_Hydrogen.h5")
        
        datafile = h5py.File(path, 'r')
        
        i = self.parameters.first_stamp + idx % self.parameters.number_per_file # every timestamp is a new date; we use stamps 7,8,9
        
        ts = list(datafile["data"].keys())[i]
        
        #print("timestamp im durchlauf " + str(i) + " ist " + str(ts))
        
        X_H, P_H = (datafile["data/" + ts + "/particles/Hydrogen/position/x"][()],
                datafile["data/" + ts + "/particles/Hydrogen/momentum/x"][()])
        
        if True: # Normalize
            X_H = X_H / 944
            P_H = (P_H + 684) / 1731
            
        pointcloud = np.stack((X_H, P_H), 0)
        # pointcloud = torch.tensor(pointcloud[:, :self.parameters.num_points])
        # pointcloud = random_sample(torch.tensor(pointcloud), self.parameters.num_points)

        pointcloud = torch.tensor(pointcloud).unsqueeze(0).transpose(1, 2)
        pointcloud = shell_sample(pointcloud, self.parameters.num_points).squeeze()
        pointcloud = pointcloud.to(device).float()
        
        # print(pointcloud.shape[0])
        

        # labels contains the values of a0, tau, n0, d, L0, L1, ts
        name_split = re.split('[-_]',sdr[idx // self.parameters.number_per_file])
        labels = name_split[1::2]
        labels.append(str(i))
        
        return [pointcloud, labels]
        