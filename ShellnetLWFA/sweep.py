#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 11:07:58 2021

@author: backdirneis
"""

print("start")

import numpy as np
import torch
import torch.nn as nn
import torch.optim
import time
import math
import os.path
import argparse
from argparse import Namespace
from pathlib import Path
from dataset import Dataset
from dataloader import create_dataloaders
from plotandsave import plot_results, save_results
from train import train
import wandb


def start_sweep(paras):
    
    """
    This function initializes the training as an agent of a sweep. Initialized 
    values may be overwritten by the values in wandb.config
    
    More detail in train()
    
    
    """
    
    paras.batchsize = 4
    paras.first_stamp = 0
    paras.number_per_file = 16
    paras.num_points = 10000
    paras.learning_rate = 0.0001
    paras.gamma = 1.0
    paras.num_downsampling = 3
    paras.num_conv_samesize = 0    
    paras.c_filters = 512  
    paras.min_neighbors = 8 
    paras.start_query = 512
    paras.latent_dim = 64
    paras.linear_layers = 3
    paras.batchnorm = True
    paras.n_epochs = 2 
    paras.normalize = True
    paras.work_local = False
    paras.fast_run = False
    paras.percentage = 20
    paras.start_from_check = False
    paras.in_team = False
    paras.skip_connect = True
    paras.xbins = 512
    paras.output_node = True
    paras.binned_loss = False
    paras.wandb = True
    paras.easy_pcs = False
    
    paras.num_points = math.floor(math.sqrt(paras.num_points))**2
    print("The number of points was corrected to: ", paras.num_points)    
        
    print("Here starts wandb as sweep!")
    run = wandb.init()
    vars(paras).update(wandb.config)

    paras.ybins = paras.xbins 
    
    print("parameters", vars(paras))
    if paras.work_local:
        paras.root_dir = "../data"
        paras.output_dir = "output/"
        print(" I shouldnt be working local! There is no data here")
    else:
        paras.root_dir = "/projects/p_da_aipp/DieckmannJakob/LWFAdata"
        paras.output_dir = "/scratch/ws/1/s5693273-output/Shellnet/LWFA/"
    
    mytime = time.strftime("%d-%m-%Y_%H:%M")
    paras.output_dir = paras.output_dir + mytime  
    print("this is the output ", paras.output_dir)
    
    wandb.config.update(vars(paras))
    
    train(run, paras, None)
    

if __name__ == "__main__":
    paras = Namespace()
    start_sweep(paras)
    
    
    