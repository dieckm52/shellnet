#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 15:17:22 2021

@author: backdirneis
"""

import torch
import os
from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np
import torch.nn as nn
import wandb
import time
import h5py
from operator import itemgetter
from helpfunctions import imshow, binned_imshow, create_labelstring, random_sample, bin_pcs
from dataloader import create_dataloaders
from dataset import Dataset
from datetime import datetime


def get_set_of_pointclouds(paras, train_loader):
    """
    This function returns a tensor with one example pointcloud of every timestamp.
    Sie number_per_file x num_points x 2

    Returns
    -------
    None.

    """
    
    #One set of training data
    labels = list()
    train_pc = torch.empty(paras.number_per_file, paras.num_points, 2) # Contains one pointcloud per timestamp
    examples = list(range(paras.first_stamp, paras.first_stamp + paras.number_per_file, 1)) # all the timestamps that should be plottet
    
    dataiter = iter(train_loader)
    batch = dataiter.next()
    k = 0
    i = 0
    while examples: # As long as we don't have an pointcloud per timestamp
        timestamp = int(batch[1][k][-2:]) 
        if timestamp in examples:
            train_pc[i] = batch[0][k] # Add sample to list of pointclouds that will be plottet
            i += 1
            examples.remove(timestamp)
            this_label = batch[1][k][-2:]
            labels.append(this_label)
        
        if k == batch[0].shape[0] - 1 and examples: # The big if else is for: Iterate through samples
            k = 0
            try:
                batch = dataiter.next()
            except: # For when data is not in training set
                print("I dont have data for this timestamp. I fill up with other data")
                timestamp = examples[0]
                train_pc[timestamp - paras.first_stamp] = batch[0][k] # Add sample to list of pointclouds that will be plottet
                examples.remove(timestamp)
                this_label = batch[1][k][-2:]
                labels.append(this_label)
        else:
            k += 1
            
        
        
    train_pc = [x for _, x in sorted(zip(labels, train_pc), key=lambda pair: pair[0])] # Complex sorting!
    labels.sort()
    
    return torch.stack(train_pc)

def save_results(model, test_loader, paras, run):
    """
    This is for saving the model aside with a certain number of test images 
    and their reconstructions. This can be used for an evaluation pipeline 
    later on. 
    

    Parameters
    ----------
    model :  Pretrained Model
        
    test_loader :  pointclouds not used for training
    
    paras : Tensor with all the parameters of the run

    Returns
    -------
    None.

    """
    
    n_test = 5
        
    h5file = paras.output_dir + ".hdf5"
    modelfile = paras.output_dir + "_model.pt"
    Path(os.path.dirname(modelfile)).mkdir(parents=True, exist_ok=True)
    torch.save(model.state_dict(), modelfile)
    
    with h5py.File(h5file, "w") as f:
        f.create_dataset("original_pcs", shape=(n_test, paras.num_points, 2))
        f.create_dataset("reconstructed_pcs", shape=(n_test, paras.num_points, 2))
        
    
        n = 0
        while n < n_test:
            for batch in test_loader:
                breakout = False
                pointclouds = batch[0].type(torch.FloatTensor).cuda()
                labels = batch[1]
                rec_pcs = model(pointclouds)
                for i in range(pointclouds.size()[0]):
                    n += 1
                    pointcloud = pointclouds[i]
                    rec_pc = rec_pcs[i]
                    if n < n_test:
                        f["original_pcs"][n] = pointcloud.cpu().detach().numpy()
                        f["reconstructed_pcs"][n] = rec_pc.detach().cpu().numpy()
                    else:
                        breakout = True
                        break
                if breakout:
                    break
    
                    
def plot_data(train_loader, paras):
    """
    This function plots a few training data for humans to get a sense of what we are 
    dealing with.

    Parameters
    ----------
    train_loader : Dataloader with training pointclouds

    Returns
    -------
    None.

    """

    train_pc = get_set_of_pointclouds(paras, train_loader)
    
    #Original pointclouds
    print("Training set")
    fig, axes = plt.subplots(nrows=1, ncols=paras.number_per_file, sharex=False, sharey=False, figsize=(paras.number_per_file*4,4))
    fig.suptitle("One example of every timestamp in training set")
    plt.subplots_adjust(wspace=0.4, hspace=0.5)
        
    for i in np.arange(paras.number_per_file):
        # Original
        if paras.number_per_file == 1: 
            imshow(train_pc[i], axes)
        else:                
            imshow(train_pc[i], axes[i])
            imshow(random_sample(train_pc[i].unsqueeze(0), paras.start_query).squeeze(), axes[i], color="red")
            
    plt.show()
        
    Path(os.path.dirname(paras.output_dir)).mkdir(parents=True, exist_ok=True)        
    plt.savefig(paras.output_dir + "_prerunplots")
    test = 5   
    print("prerunplots available now")     

        

def plot_results(model, train_loader, test_loader, paras):
    """
    This function plots some training and test pointclouds above their reconstructed
    counterparts. This is for visually evaluate reconstruction quality. 

    Parameters
    ----------
    model : Trained model to calculate reconstruction.
    train_loader : Dataloader with train pointclouds.
    test_loader : Dataloader with test pointclouds.
    paras : Tensor with all the parameters of the run.

    Returns
    -------
    None.

    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # number_per_file = 10 - paras.first_stamp


    print("plotresult starts")
    
    
    critirion = paras.criterion
    
    batch = iter(train_loader).next()
    train_pc = batch[0].cpu()
    train_label = batch[1]
     
    batch = iter(test_loader).next()
    test_pc = batch[0].cpu()
    test_label = batch[1]
    
       
    # Sample outputs
    train_output = model(train_pc.to(device)).detach().cpu()
    test_output = model(test_pc.to(device)).detach().cpu()
    
    number_printed = paras.batchsize

    print("Training set")
    fig, axes = plt.subplots(nrows=2, ncols=number_printed, sharex=False, sharey=False, figsize=(number_printed*4,12))
    fig.suptitle("One example of every timestamp in training set \nFirst row Originals, second row reconstructed pointclouds")
    

    
    for i in np.arange(number_printed):
        # Original
        if number_printed == 1: 
            imshow(train_pc[i], axes[0])
            imshow(train_output[i], axes[1])
        else:                
            imshow(train_pc[i], axes[0, i])
            imshow(train_output[i], axes[1, i])
            axes[1, i].title.set_text("Loss: %2.3f" % critirion(train_pc[i], train_output[i]))
    Path(os.path.dirname(paras.output_dir)).mkdir(parents=True, exist_ok=True)        
    plt.savefig(paras.output_dir + "_train_set")
    
    print("Test set")
    fig, axes = plt.subplots(nrows=2, ncols=number_printed, sharex=False, sharey=False, figsize=(number_printed*4,12))
    fig.suptitle("One example of every timestamp in test set \nFirst row Originals, second row reconstructed pointclouds")
    
    
    for i in np.arange(number_printed):
        # Original
        if number_printed == 1: 
            imshow(test_pc[i], axes[0])
            imshow(test_output[i], axes[1])
        else:                
            imshow(test_pc[i], axes[0, i])
            imshow(test_output[i], axes[1, i])
            axes[1, i].title.set_text("Loss: %2.3f" % critirion(test_pc[i], test_output[i]))
    Path(os.path.dirname(paras.output_dir)).mkdir(parents=True, exist_ok=True)        
    plt.savefig(paras.output_dir + "_test_set") 
    
    print("binned_test set")
    fig, axes = plt.subplots(nrows=2, ncols=number_printed, sharex=False, sharey=False, figsize=(number_printed*4,12))
    fig.suptitle("One example of every timestamp in test set \nFirst row Originals, second row reconstructed pointclouds")
    
    bin_test = bin_pcs(test_pc)
    bin_test_out = bin_pcs(test_output)
   
    
    for i in np.arange(number_printed):
        # Original
        if number_printed == 1: 
            binned_imshow(bin_test[i], axes[0])
            binned_imshow(bin_test_out[i], axes[1])
        else:                
            binned_imshow(bin_test[i], axes[0, i])
            binned_imshow(bin_test_out[i], axes[1, i])
            axes[1, i].title.set_text("Binned Loss: %2.3f" % critirion(test_pc[i], test_output[i]))
    Path(os.path.dirname(paras.output_dir)).mkdir(parents=True, exist_ok=True)        
    plt.savefig(paras.output_dir + "_bin_test_set")   
    
    if paras.wandb:
        wandb.log({"test": wandb.Image(paras.output_dir + "_test_set.png"), 
                   "train": wandb.Image(paras.output_dir + "_train_set.png"),
                   "bin_test": wandb.Image(paras.output_dir + "_bin_test_set.png")})
